package mx.edu.ittepic.tpdm_u3_ejercicio1;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText numero;
    Button ciclo, hilo, timer;
    CountDownTimer timercito;
    Thread hilito;
    int conta, contador2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numero = findViewById(R.id.numero);
        ciclo = findViewById(R.id.ciclo);
        hilo = findViewById(R.id.hilo);
        timer = findViewById(R.id.timer);

        ciclo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=1; i<10000; i++){
                    numero.setText(""+i);
                }
            }
        });

        timercito = new CountDownTimer(1000000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                conta++;
                numero.setText(""+conta);
            }

            @Override
            public void onFinish() {

            }
        };

        timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timercito.start();
            }
        });

        hilito = new Thread(){
            public void run(){
                while(true){
                    try {
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    contador2++;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            numero.setText(""+contador2);
                        }
                    });
                }
            }
        };

        hilo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilito.start();
            }
        });
    }
}
